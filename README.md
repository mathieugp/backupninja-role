Backupninja 
=========

Ce rôle sert à configurer les sauvegardes automatiques avec backupninja (sqldump+rdiff).

Requirements
------------

### Système d'exploitation
* Debian et dérivés

Role Variables
--------------

### defaults/main.yml

Les valeurs par défaut dans defaults/main.yml qu'il est normal d'adapter sont l'utilisateur unix, le serveur de sauvegarde et les répertoires/fichiers à inclure ou exclude des sauvegardes. Vous pouvez les modifier directement dans main.yml ou mieux encore les supplanter ailleurs (par exemple dans votre playbook site.yml).

Dependencies
------------

Aucune dépendance à d'autres rôles Ansible.

Example Playbook
----------------

```
- name: "Configurer les sauvegardes automatiques avec backupninja (sqldump+rdiff)"
  hosts: 
    - all
  vars:
    # Supplanter ici les variables du role au besoin
  
  roles:
    - { role: backupninja-role, tags: "backupninja" }
```

License
-------

GPL-3.0-only

Author Information
------------------

Mathieu GP
